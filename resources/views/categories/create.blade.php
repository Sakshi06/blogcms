P@extends('layouts.admin-panel.app')


@section('content')



    <div class="card">
        <div class="card-header m-0">
            Add a new category
        </div>
        <div class="card-body">
            <form action="{{ route('categories.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control @error('name')
                                                                                is-invalid
                                                                               @enderror" value="{{ old('name') }}"
                        placeholder="Enter category name">
                    @error('name')
                        <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <button type="submit" class="btn btn-outline-success">Submit</button>
            </form>
        </div>
    </div>
@endsection
