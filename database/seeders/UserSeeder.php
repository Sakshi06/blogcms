<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Sakshi Khandekar',
            'email' => 'Sakshi@gmail.com',
            'password' => Hash::make('abcd1234')
        ]);

        User::create([
            'name' => 'Sayal',
            'email' => 'Sayali@gmail.com',
            'password' => Hash::make('abcd1234')
        ]);
    }
}
